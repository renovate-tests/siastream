/* eslint-disable no-unused-expressions */
const assert = require("assert");
const fs = require("fs");
const os = require("os");
const path = require("path");
const AdmZip = require("adm-zip");
const execa = require("execa");
const ora = require("ora");
const pkg = require("pkg");
const semver = require("semver");
const yargs = require("yargs");

const nodeVersion = "12";
const arch = "x64";

// current package can be built only on x64 arch with node 12 (compatibility)
assert(semver.satisfies(process.versions.node, nodeVersion));
assert(os.arch() === arch);

yargs
  .command(
    "$0 <platform>",
    "Compile a SiaStream package for a specific platform",
    () => {
      yargs.positional("platform", {
        describe: "Target platform",
        choices: ["linux", "macos"],
      });
    },
    async ({ platform }) => {
      const dist = path.join("dist", `siastream-${platform}-${arch}`);

      await fs.promises.rmdir(dist, { recursive: true });
      await build(dist, platform);

      if (platform === "linux") {
        await copyXDGOpen(dist);
      }
      if (platform === "macos") {
        await copyFSEvents(dist);
      }
      await copyGeoipLiteData(dist);
      await createZipArchive(dist);
    }
  )
  .help().argv;

async function build(dist, platform) {
  await pkg.exec([
    path.join("build-server", "cli.js"),
    "--config",
    "pkg.config.json",
    "--target",
    `node${nodeVersion}-${platform}-${arch}`,
    "--output",
    path.join(dist, "siastream"),
  ]);
  ora({ prefixText: `Building ${platform} package` }).succeed("Done!");
}

async function copyGeoipLiteData(dist) {
  const spinner = ora({ prefixText: "Copying geoip-lite data" }).start();
  const source = path.join("node_modules", "geoip-lite", "data");
  const target = path.join(dist, "node_modules", "geoip-lite");

  await fs.promises.mkdir(target, { recursive: true });
  await execa("cp", ["-R", source, target]);
  spinner.succeed("Done!");
}

async function copyXDGOpen(dist) {
  const spinner = ora({ prefixText: "Copying xdg-open" }).start();
  const source = path.join("node_modules", "open", "xdg-open");
  const target = path.join(dist, "node_modules", "open");

  await fs.promises.mkdir(target, { recursive: true });
  await execa("cp", [source, target]);
  spinner.succeed("Done!");
}

async function copyFSEvents(dist) {
  const spinner = ora({ prefixText: "Copying fsevents" }).start();
  const source = path.join("node_modules", "chokidar", "node_modules", "fsevents", "fsevents.node");
  const target = path.join(dist, "node_modules", "fsevents");

  await fs.promises.mkdir(target, { recursive: true });
  await execa("cp", [source, target]);
  spinner.succeed("Done!");
}

async function createZipArchive(dist) {
  const spinner = ora({ prefixText: "Creating zip archive" }).start();
  const zip = new AdmZip();

  zip.addLocalFolder(dist);
  zip.writeZip(`${dist}.zip`);

  spinner.succeed("Done!");
}
