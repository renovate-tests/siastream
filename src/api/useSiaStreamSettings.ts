import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

type SettingsName = "stream" | "allowance" | "siad";

/**
 * SWR wrapped react hook fetching one of the settings endpoints from the server
 */
export default function useSiaStreamSettings(name: SettingsName, config?: ConfigInterface) {
  return useSWR<any>(`/api/config/${name}`, fetcher, config);
}
