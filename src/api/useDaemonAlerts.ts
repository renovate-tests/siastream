import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /daemon/alerts endpoint react hook for daemon alerts data fetching
 * https://sia.tech/docs/#daemon-alerts-get
 */
export default function useDaemonAlerts(config?: ConfigInterface) {
  return useSWR<any>("/siad/daemon/alerts", fetcher, config);
}
