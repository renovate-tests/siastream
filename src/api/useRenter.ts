import { RenterGET } from "sia-typescript";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /renter endpoint react hook for renter data fetching
 * https://sia.tech/docs/#renter-get
 */
export default function useRenter(config?: ConfigInterface) {
  return useSWR<RenterGET>("/siad/renter", fetcher, config);
}
