import { ConsensusGET } from "sia-typescript";
import useSWR, { ConfigInterface } from "swr";
import fetcher from "./fetcher";

/**
 * SWR wrapped Sia GET /consensus endpoint react hook for consensus data fetching
 * https://sia.tech/docs/#consensus-get
 */
export default function useConsensus(config?: ConfigInterface) {
  return useSWR<ConsensusGET>("/siad/consensus", fetcher, config);
}
