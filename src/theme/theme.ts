export const colors = {
  text: "#FFFFFF",
  background: "#171717",
  card: "#1F1F1F",

  primary: "#1DB954",
  primaryContrast: "#000",
  secondary: "#30c",
  muted: "#f6f6f6",
  warning: "#ff9800",
  error: "#f44336",
};

const heading = {
  fontFamily: "heading",
  lineHeight: "heading",
  fontWeight: "heading",
  marginBlockStart: 0,
  marginBlockEnd: 0,
};

export const base = {
  breakpoints: ["40em", "52em", "64em"],
  space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
  fonts: {
    body: "Metropolis, system-ui, Sans-Serif",
    heading: "Metropolis, system-ui, Sans-Serif",
    monospace: "Menlo, monospace",
  },
  fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 96],
  radii: [0, 4, 16],
  fontWeights: {
    body: 500,
    heading: 700,
    bold: 600,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.24,
  },
  colors: {
    ...colors,
  },
  tags: {
    normal: {
      px: 3,
      py: 2,
    },
    small: {
      px: 3,
      py: "2px",
    },
  },
  buttons: {
    primary: {
      color: "primaryContrast",
      bg: "primary",
    },
    secondary: {
      color: "background",
    },
    error: {
      color: "text",
      bg: "gray",
    },
    disabled: {
      color: "background",
      bg: "gray",
      cursor: "default",
    },
  },
  toggles: {
    normal: {
      width: 60,
      height: 34,
    },
    small: {
      width: 60,
      height: 28,
    },
  },

  styles: {
    root: {
      fontFamily: "body",
      fontWeight: "body",
      lineHeight: "body",
      color: "text",
    },
    p: {
      fontSize: 2,
    },
    a: {
      color: colors.text,
      ":hover": {
        textDecoration: "underline",
      },
    },
    h1: {
      ...heading,
      fontSize: 5,
    },
    h2: {
      ...heading,
      fontSize: 4,
    },
    h3: {
      ...heading,
      fontSize: 3,
    },
    h4: {
      ...heading,
      fontSize: 2,
      fontWeight: 400,
      letterSpacing: 1.2,
    },
    h5: {
      ...heading,
      fontSize: 1,
    },
    h6: {
      ...heading,
      fontSize: 0,
    },
    pre: {
      fontFamily: "monospace",
      overflowX: "auto",
      code: {
        color: "inherit",
      },
    },
    code: {
      fontFamily: "monospace",
      fontSize: "inherit",
    },
    table: {
      width: "100%",
      borderCollapse: "separate",
      borderSpacing: 0,
    },
    th: {
      textAlign: "left",
      borderBottomStyle: "solid",
    },
    td: {
      textAlign: "left",
      borderBottomStyle: "solid",
    },
  },
};
