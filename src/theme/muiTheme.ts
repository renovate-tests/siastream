import { createMuiTheme } from "@material-ui/core";
import { makeStyles, Theme } from "@material-ui/core/styles";

export const useSnackbarStyles = makeStyles((theme: Theme) => ({
  variantSuccess: {
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${theme.palette.success.main}`,
  },
  variantError: {
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${theme.palette.error.main}`,
  },
  variantWarning: {
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${theme.palette.warning.main}`,
  },
  variantInfo: {
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${theme.palette.info.main}`,
  },
}));

export default createMuiTheme({
  typography: {
    fontFamily: "Metropolis, system-ui, Sans-Serif",
    h1: { fontSize: 32, lineHeight: 1.24, fontWeight: 700 },
    h2: { fontSize: 24, lineHeight: 1.24, fontWeight: 700 },
    h3: { fontSize: 18, lineHeight: 1.24, fontWeight: 700 },
    h4: { fontSize: 16, lineHeight: 1.24, fontWeight: 700 },
  },
  palette: {
    type: "dark",
    primary: { main: "#1db954" },
    background: { default: "#171717", paper: "#1F1F1F" },
    warning: { main: "#E7B448" },
  },
  overrides: {
    MuiPaper: {
      rounded: {
        borderRadius: 16,
      },
    },
    MuiButton: {
      root: {
        borderRadius: 16,
        padding: "9px 24px",
      },
      contained: {
        fontWeight: "bold",
      },
    },
    MuiChip: {
      root: {
        height: 42,
        minWidth: 64,
        padding: "9px 12px",
        fontSize: "14px",
        backgroundColor: "#e0e0e0",
        color: "#000",
      },
      colorPrimary: {
        fontWeight: "bold",
      },
      label: {
        lineHeight: "24.5px",
      },
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: 16,
      },
    },
    MuiTypography: {
      paragraph: {
        marginBottom: 24,
      },
      gutterBottom: {
        marginBottom: 24,
      },
    },
    MuiSnackbarContent: {
      root: {
        backgroundColor: "#1F1F1F",
        color: "#fff",
      },
    },
  },
  props: {
    MuiTextField: {
      variant: "filled",
    },
    MuiFilledInput: {
      disableUnderline: true,
    },
  },
});
