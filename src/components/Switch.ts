import { Switch } from "@material-ui/core";
import { withStyles, Theme, createStyles } from "@material-ui/core/styles";

export default withStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 56,
      height: 34,
      padding: 0,
      margin: theme.spacing(1),
    },
    switchBase: {
      padding: 4,
      "&$checked + $track": {
        opacity: 1,
      },
    },
    thumb: {
      width: 26,
      height: 26,
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
    },
    track: {
      borderRadius: 4,
    },
    checked: {},
  })
)(Switch);
