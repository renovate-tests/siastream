/** @jsx jsx */
import css from "@emotion/css";
import { Suspense, Fragment } from "react";
import { Box, jsx } from "theme-ui";
import { CardFluid, CardHeader, CardTitle } from "../components/Card";
import { cssDefaultGrid } from "../components/styles";
import { DepositAddressModal } from "./DepositAddressModal";
import { FuseCard } from "./FuseCard";
import { SpendingPie } from "./Graphs";
import { WalletCard } from "./WalletCard";
import { WalletUnlockModal } from "./WalletUnlockModal";

export const Renter = () => {
  return (
    <Fragment>
      <WalletUnlockModal />
      <DepositAddressModal />
      <Box
        css={css`
          ${cssDefaultGrid}
          grid-template-rows: 1fr 1fr;
        `}
        sx={{ pb: "32px" }}
      >
        <WalletCard
          css={css`
            grid-column-start: 1;
            grid-column-end: 3;
            grid-row-start: 1;
            grid-row-end: 2;
          `}
        />
        <Suspense fallback={null}>
          <FuseCard />
        </Suspense>
        <CardFluid
          css={css`
            grid-column-start: 3;
            grid-column-end: 5;
            grid-row-start: 1;
            grid-row-end: 3;
          `}
        >
          <CardHeader>
            <CardTitle>Spending Overview</CardTitle>
          </CardHeader>
          <Box sx={{ height: "calc(100% - 60px)", p: 4 }}>
            <div
              sx={{
                position: "relative",
                height: 340,
                mt: "auto",
                top: "50%",
                transform: "translateY(-50%)",
              }}
            >
              <SpendingPie />
            </div>
          </Box>
        </CardFluid>
      </Box>
    </Fragment>
  );
};
