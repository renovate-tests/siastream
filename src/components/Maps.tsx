import { ArcLayer } from "@deck.gl/layers";
import DeckGL from "@deck.gl/react";
import React, { useMemo } from "react";
import { useFetch } from "react-async";
import { StaticMap } from "react-map-gl";

// Initial viewport settings
const initialViewState = {
  longitude: 30.41669,
  latitude: 23.7853,
  zoom: 0.6,
  maxZoom: 4,
  minZoom: 0,
  pitch: 20,
  bearing: 0,
};

export const HostMap = (p) => {
  const mapData = useFetch("/api/geolocation", {
    headers: {
      Accept: "application/json",
    },
  });
  const layers = useMemo(
    () => [
      new ArcLayer({
        id: "arc-layer",
        data: mapData.data || [],
        pickable: true,
        getWidth: 1,
        getHeight: 0.4,
        getSourcePosition: (d) => d.source,
        getTargetPosition: (d) => d.target,
        getSourceColor: [20, 205, 171],
        getTargetColor: [80, 235, 93],
        // getSourceColor: d => [Math.sqrt(d.source), 140, 0],
        // getTargetColor: d => [Math.sqrt(d.target), 140, 0]
        // onHover: ({ object, x, y }) => {
        //   const tooltip = `${object.from.name} to ${object.to.name}`
        //   /* Update tooltip
        //    http://deck.gl/#/documentation/developer-guide/adding-interactivity?section=example-display-a-tooltip-for-hovered-object
        // */
        // }
      }),
    ],
    [mapData.data]
  );
  return (
    <DeckGL initialViewState={initialViewState} controller={true} layers={layers} {...p}>
      <StaticMap mapboxApiUrl="https://maps.sia.tech" mapStyle="https://maps.sia.tech/styles/dark-matter/style.json" />
    </DeckGL>
  );
};
