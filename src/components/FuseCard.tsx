import css from "@emotion/css";
import { Box, Link, Typography, CircularProgress } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import ms from "ms";
import React, { useState, useEffect } from "react";
import { RenterFuse } from "sia-typescript";
import { useFuse, useOS, useSiaStreamSettings } from "../api";
import { CardBody, CardFluid, CardHeader, CardTitle } from "../components/Card";
import Switch from "../components/Switch";
import { Tag } from "../components/Tag";
import { Caps } from "../components/Typography";
import { useDBConfig } from "../states/DBConfig";

function FuseLoading() {
  const [retries, setRetries] = useState(0);
  const [error, setError] = useState("");
  const { data: os } = useOS({ suspense: true });
  const { data: streamConfig } = useSiaStreamSettings("stream", { suspense: true });

  const isLinuxServer = os.platform === "linux";
  const isMacServer = os.platform === "darwin";

  useFuse({
    suspense: true,
    refreshInterval: ms("3 seconds"),
    onSuccess: async (fuse: RenterFuse) => {
      const isFuseMounted = Boolean(fuse?.mountpoints?.length);

      // increment the retry counter every time the api responds with negative result and clear otherwise
      if (streamConfig.mountFuse === isFuseMounted) {
        setRetries(0);
      } else {
        setRetries((retries) => retries + 1);
      }
    },
  });

  // after a couple of checks where siad still didn't respond with the expected result, we can assume something
  // is wrong with fuse on the server and while logs are available in the server console, we want to try
  // manually mounting/unmounting the fuse path to print any errors to user in browser
  useEffect(() => {
    if (retries === 5) {
      axios.post(`/siad/fuse/${streamConfig.mountFuse ? "mount" : "unmount"}`).catch((error) => {
        setError(error.response ? error.response.data.message : error.message);
      });
    }
  }, [retries, streamConfig.mountFuse]);

  return (
    <Box textAlign="center">
      {error && (
        <Box textAlign="center">
          <Box mb={2} color="error.main">
            <Alert variant="outlined" icon={false} color="error">
              The FUSE drive failed to mount: {error}
            </Alert>
          </Box>

          {/* additional information for macos users */}
          {isMacServer && (
            <Typography variant="body2" color="textSecondary">
              If you are using MacOS, please make sure{" "}
              <Link href="https://osxfuse.github.io/" target="_blank" rel="noopener noreferrer">
                FUSE
              </Link>{" "}
              is installed.
            </Typography>
          )}

          {/* additional information for linux users */}
          {isLinuxServer && (
            <Typography variant="body2" color="textSecondary">
              If you are running Sia on a non-root user, make sure you enable user_allow_other in /etc/fuse.conf.
            </Typography>
          )}
        </Box>
      )}

      {!error && <CircularProgress />}

      {retries > 10 && !error && (
        <Typography variant="body2" color="textSecondary">
          This is taking longer than usual. Please check SiaStream console for possible additional information.
        </Typography>
      )}
    </Box>
  );
}

function FuseDetails() {
  const { data: fuse } = useFuse({ suspense: true });
  const isFuseMounted = Boolean(fuse?.mountpoints?.length);

  if (isFuseMounted) {
    return (
      <Box textAlign="center">
        <Caps>Mounted To:</Caps>
        {fuse.mountpoints.map(({ mountpoint }, index: number) => (
          <Tag key={index} color="background" sx={{ mt: 2 }}>
            {mountpoint}
          </Tag>
        ))}
      </Box>
    );
  }

  return <Box textAlign="center">The FUSE drive is not mounted.</Box>;
}

export const FuseCard = () => {
  const { data: fuse } = useFuse();
  const { data: streamConfig, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", { suspense: true });
  const { setStreamConfig } = useDBConfig();

  const isFuseMounted = Boolean(fuse?.mountpoints?.length);

  const handleFuseToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
    mutateStreamConfig(setStreamConfig({ mountFuse: event.target.checked }));
  };

  return (
    <CardFluid
      css={css`
        grid-column-start: 1;
        grid-column-end: 3;
        grid-row-start: 2;
        grid-row-end: 3;
      `}
    >
      <CardHeader>
        <CardTitle sx={{ alignSelf: "center" }}>FUSE Status</CardTitle>
        <Switch color="primary" checked={streamConfig.mountFuse} onChange={handleFuseToggle} />
      </CardHeader>
      <React.Suspense fallback={<FuseLoading />}>
        <CardBody>{streamConfig.mountFuse === isFuseMounted ? <FuseDetails /> : <FuseLoading />}</CardBody>
      </React.Suspense>
    </CardFluid>
  );
};
