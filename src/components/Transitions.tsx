export const AnimFadeIn = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
  },
};

export const AnimFadeInUp = {
  initial: {
    opacity: 0,
    y: 50,
  },
  animate: {
    opacity: 1,
    y: 0,
  },
};

export const AnimScalePress = {
  whileHover: {
    scale: 0.98,
  },
  whileTap: {
    y: 4,
  },
};

export const VariantStaggeredFadeIn = {
  hidden: {
    opacity: 0,
    transition: {
      when: "afterChildren",
    },
  },
  visible: {
    opacity: 1,
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
      delay: 0.5,
    },
  },
};

// Used in staggered transitions on the Onboarding flow.
export const VariantFadeInUp = {
  visible: {
    opacity: 1,
    y: 0,
  },
  hidden: {
    opacity: 0,
    y: 50,
  },
};

export const VariantSlideInOut = {
  hidden: {
    display: "none",
    opacity: 0,
    x: 1000,
  },
  visible: {
    display: "block",
    opacity: 1,
    x: 0,
    transition: {
      when: "beforeChildren",
      delay: 0.4,
    },
  },
  exit: {
    opacity: 0,
    x: -1000,
    transition: {
      duration: 0.4,
    },
    transitionEnd: { display: "none" },
  },
};

export const VariantRightContextMenu = {
  hidden: {
    right: -500,
  },
  visible: {
    right: 0,
  },
};
