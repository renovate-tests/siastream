/** @jsx jsx */
import css from "@emotion/css";
import { motion } from "framer-motion";
import { lighten, transparentize } from "polished";
import { Box, jsx } from "theme-ui";
import { colors } from "../theme";
import { Tag } from "./Tag";
import { Caps } from "./Typography";

export const CardBody = ({ sx = {}, ...p }) => <Box sx={{ ...{ height: "calc(100% - 60px)", p: 48 }, ...sx }} {...p} />;

export const Card = ({ sx = {}, ...p }) => (
  <motion.div
    sx={{
      ...{
        position: "relative",
        backgroundColor: "card",
        padding: 48,
        borderRadius: 2,
        h2: {
          margin: 0,
        },
        h3: {
          margin: 0,
        },
      },
      ...sx,
    }}
    {...p}
  />
);

export const CardFluid = (p) => <Card sx={{ padding: 0 }} {...p} />;

export const CardHeader = (p) => {
  return (
    <div>
      <Box
        css={css`
          position: absolute;
          display: flex;
          align-items: center;
          top: 0;
          left: 0;
          height: 60px;
          width: 100%;
          border-bottom: 1px solid ${transparentize(0.4, lighten(0.2, colors.card))};
        `}
      >
        <Box
          sx={{
            display: "flex",
            width: "100%",
            px: 32,
            justifyContent: "space-between",
            alignItems: "center",
          }}
          {...p}
        />
      </Box>
      <div sx={{ pt: 60 }} />
    </div>
  );
};

export const CardTitle = (p) => (
  <div>
    <Caps {...p} />
  </div>
);

export const CardTag = (p) => (
  <div>
    <Tag variant="small" color={p.color} sx={{ py: 1 }}>
      <span
        sx={{
          fontSize: 0,
          fontWeight: "bold",
          letterSpacing: 1.2,
          textTransform: "uppercase",
          lineHeight: 1,
        }}
        {...p}
      />
    </Tag>
  </div>
);
