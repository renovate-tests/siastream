import { Box, IconButton, Tooltip, Button, Typography } from "@material-ui/core";
import { HelpOutline } from "@material-ui/icons";
import { Alert } from "@material-ui/lab";
import { round } from "lodash";
import ms from "ms";
import React, { useContext } from "react";
import { toSiacoins, BYTES_IN_TERABYTE } from "sia-typescript";
import { usePricing, useWallet, useRenterContracts, useSiaStreamSettings } from "../api";
import { STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH } from "../constants";
import { DispatchContext } from "../states/Context";
import { formatNumber } from "../util/utils";
import { CardFluid, CardHeader, CardTitle } from "./Card";
import { Tag } from "./Tag";

function DepositButton({ disabled, dispatch }) {
  const DepositButton = (
    <Button
      variant="contained"
      color="primary"
      disabled={disabled}
      onClick={() => dispatch({ type: "DEPOSIT_DIALOG/OPEN" })}
    >
      Deposit
    </Button>
  );

  if (disabled) {
    return (
      <Tooltip title="Deposit it disabled during wallet rescan, please wait">
        <span>{DepositButton}</span>
      </Tooltip>
    );
  }

  return DepositButton;
}

export const WalletCard = (props) => {
  const { usdRate } = usePricing();
  const dispatch = useContext(DispatchContext);
  const { data: allowanceConfig } = useSiaStreamSettings("allowance", { suspense: true });
  const { data: wallet } = useWallet({ refreshInterval: ms("5 seconds") });
  const { data: renterContracts } = useRenterContracts();
  const siacoinBN = toSiacoins(wallet?.confirmedsiacoinbalance ?? 0);
  const siacoinString = formatNumber(siacoinBN.toFixed(2));
  const usd = siacoinBN.multipliedBy(usdRate as number);

  const expectedStorageTerabytes = round(allowanceConfig.expectedstorage / BYTES_IN_TERABYTE, 2);
  const monthlySiaStreamPrice = expectedStorageTerabytes * STORAGE_PRICE_USD_PER_TERABYTE_PER_MONTH;
  const initialSiaStreamPrice = monthlySiaStreamPrice * 3;

  const showInitialDepositMessage = siacoinBN.isZero() && renterContracts?.activecontracts === null;

  return (
    <CardFluid {...props}>
      <CardHeader>
        <CardTitle>Balance</CardTitle>
        <Box display="flex" alignItems="center">
          <Tooltip title="How to buy Siacoins" aria-label="How to buy Siacoin">
            <IconButton
              aria-label="How to buy Siacoin?"
              component="a"
              href="https://support.siastream.tech/article/xzl614rndr-how-to-buy"
              target="_blank"
            >
              <HelpOutline />
            </IconButton>
          </Tooltip>

          <DepositButton disabled={wallet?.rescanning} dispatch={dispatch} />
        </Box>
      </CardHeader>
      <Box p={4}>
        <Box>
          <Tag color="background">
            <Typography variant="h3">{siacoinString} SC</Typography>
          </Tag>
        </Box>
        <Box display="flex" justifyContent="center" mt={3}>
          <Tag variant="small" color="transparent">
            <Typography variant="h4">${usd.toFixed(2)} USD</Typography>
          </Tag>
        </Box>
        {showInitialDepositMessage && (
          <Box mt={3}>
            <Alert variant="outlined" icon={false} color="warning">
              Based on your estimated storage size of {expectedStorageTerabytes} TB, you should initially fund your
              wallet with at least ${initialSiaStreamPrice}.
            </Alert>
          </Box>
        )}
      </Box>
    </CardFluid>
  );
};
