import {
  Button,
  FormControlLabel,
  Card,
  CardContent,
  CardActions,
  Typography,
  Box,
  TextField,
  Link,
  Checkbox,
  FormHelperText,
  InputAdornment,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { useFormik } from "formik";
import { motion } from "framer-motion";
import { useSnackbar } from "notistack";
import React, { useState } from "react";
import { BYTES_IN_TERABYTE } from "sia-typescript";
import * as Yup from "yup";
import { useSiaStreamSettings } from "../api";
import { AnimFadeInUp } from "../components/Transitions";
import { useDBConfig } from "../states/DBConfig";

export const SettingsForm = () => {
  const { data: streamConfig, mutate: mutateStreamConfig } = useSiaStreamSettings("stream", { suspense: true });
  const { data: allowanceConfig, mutate: mutateAllowanceConfig } = useSiaStreamSettings("allowance", {
    suspense: true,
  });
  const { setStreamConfig, setRenterConfig } = useDBConfig();
  const [showAdvanced, setShowAdvanced] = useState(false);
  const { enqueueSnackbar } = useSnackbar();
  const formik = useFormik({
    initialValues: {
      mediaRootDirectory: streamConfig.localPath,
      fuseMountPath: streamConfig.fuseMountPath,
      estimateSize: allowanceConfig.expectedstorage / BYTES_IN_TERABYTE,
      siaStreamSiaPath: streamConfig.siaStreamSiaPath,
      automaticAllowance: streamConfig.automaticAllowance,
      removeMediaAfterUpload: streamConfig.removeMediaAfterUpload,
    },
    validationSchema: Yup.object({
      mediaRootDirectory: Yup.string().required("Media root directory cannot be empty"),
      fuseMountPath: Yup.string().required("FUSE mount path cannot be empty"),
      estimateSize: Yup.number().min(1).max(20).required("Estimated media library size cannot be empty"),
      siaStreamSiaPath: Yup.string().required("Sia path cannot be empty"),
      automaticAllowance: Yup.boolean(),
      removeMediaAfterUpload: Yup.boolean(),
    }),
    onSubmit: async (values, { setSubmitting }) => {
      try {
        await mutateStreamConfig(
          setStreamConfig({
            localPath: values.mediaRootDirectory,
            fuseMountPath: values.fuseMountPath,
            siaStreamSiaPath: values.siaStreamSiaPath,
            automaticAllowance: values.automaticAllowance,
            removeMediaAfterUpload: values.removeMediaAfterUpload,
          })
        );
        await mutateAllowanceConfig(
          setRenterConfig({
            expectedstorage: values.estimateSize * BYTES_IN_TERABYTE,
          })
        );
      } catch (error) {
        enqueueSnackbar(`Error setting SiaStream config: ${error.message}`, { variant: "error" });
      }

      setSubmitting(false);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Card>
        <CardContent>
          <Box mb={6}>
            <Typography variant="h3" paragraph gutterBottom>
              Media Root Directory
            </Typography>

            <Typography paragraph>
              SiaStream will automatically monitor this path and upload its contents to the Sia network. Choose the
              existing directory where your media is stored.
            </Typography>

            <Box mb={3}>
              <TextField
                name="mediaRootDirectory"
                label="Media Root Directory"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.mediaRootDirectory && Boolean(formik.errors.mediaRootDirectory)}
                helperText={formik.touched.mediaRootDirectory && formik.errors.mediaRootDirectory}
                value={formik.values.mediaRootDirectory}
              />
            </Box>

            <Box mb={3}>
              <TextField
                name="estimateSize"
                label="Estimated Media Library Size"
                fullWidth
                InputProps={{
                  endAdornment: <InputAdornment position="end">TB</InputAdornment>,
                }}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.estimateSize && Boolean(formik.errors.estimateSize)}
                helperText={formik.touched.estimateSize && formik.errors.estimateSize}
                value={formik.values.estimateSize}
              />
            </Box>
          </Box>

          <Box mb={6}>
            <Box display="flex" justifyContent="space-between" alignItems="center" mb={3}>
              <Typography variant="h3">FUSE Mount Path</Typography>

              <Link
                href="https://support.siastream.tech/article/p4kecgw0ab-siastream-fuse"
                target="_blank"
                rel="noopener noreferrer"
              >
                What is FUSE?
              </Link>
            </Box>

            <Typography paragraph>
              This is where SiaStream will mount the virtual drive with your uploaded media. You will add this path to
              Plex and other media apps to stream from.
            </Typography>

            <Box mb={3}>
              <TextField
                name="fuseMountPath"
                label="FUSE Mount Path"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.fuseMountPath && Boolean(formik.errors.fuseMountPath)}
                helperText={formik.touched.fuseMountPath && formik.errors.fuseMountPath}
                value={formik.values.fuseMountPath}
              />
            </Box>
          </Box>

          <Box mb={6}>
            <Typography variant="h3" paragraph gutterBottom>
              Sia Path
            </Typography>

            <Typography paragraph>
              Set the path where Sia stores your media. This is the root path for where all your SiaStream data is
              stored on the Sia Network. We recommend leaving this unchanged unless <b>/siastream</b> is already in use.
            </Typography>

            <TextField
              name="siaStreamSiaPath"
              label="Sia Path"
              fullWidth
              disabled
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={formik.touched.siaStreamSiaPath && Boolean(formik.errors.siaStreamSiaPath)}
              helperText={formik.touched.siaStreamSiaPath && formik.errors.siaStreamSiaPath}
              value={formik.values.siaStreamSiaPath}
            />

            <FormHelperText>
              ⚠ Changing Sia Path is temporarily disabled due to{" "}
              <Link
                href="https://gitlab.com/NebulousLabs/siastream/-/issues/85"
                target="_blank"
                rel="noopener noreferrer"
              >
                issue #85
              </Link>
            </FormHelperText>
          </Box>

          <Box>
            {showAdvanced && (
              <motion.div {...AnimFadeInUp}>
                <Typography variant="h3" paragraph gutterBottom>
                  Advanced Settings
                </Typography>

                <Box mb={3}>
                  <Alert severity="warning" variant="outlined">
                    SiaStream is still in beta version
                  </Alert>
                </Box>

                <Box mb={3}>
                  <Box mb={3}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          name="automaticAllowance"
                          color="primary"
                          onChange={formik.handleChange}
                          checked={formik.values.automaticAllowance}
                        />
                      }
                      label="Allow SiaStream to manage your allowance settings"
                    />
                    <FormHelperText>
                      NOTE: this means SiaStream will override any allowance changes that are made outside of SiaStream.
                    </FormHelperText>
                  </Box>

                  <FormControlLabel
                    control={
                      <Checkbox
                        name="removeMediaAfterUpload"
                        color="primary"
                        onChange={formik.handleChange}
                        checked={formik.values.removeMediaAfterUpload}
                      />
                    }
                    label="Automatically delete local files once uploaded to Sia."
                  />
                </Box>
              </motion.div>
            )}
          </Box>
        </CardContent>
        <CardActions style={{ justifyContent: "flex-end" }}>
          {!showAdvanced && (
            <Box mr="auto">
              <Button variant="contained" onClick={() => setShowAdvanced(!showAdvanced)}>
                Show advanced settings
              </Button>
            </Box>
          )}

          <Button disabled={!formik.dirty} variant="contained" onClick={() => formik.resetForm()}>
            Discard Changes
          </Button>

          <Button type="submit" variant="contained" color="primary" disabled={!formik.dirty}>
            {formik.status === "error" ? "Retry" : "Save"}
          </Button>
        </CardActions>
      </Card>
    </form>
  );
};
