/** @jsx jsx */
import { ResponsivePie } from "@nivo/pie";
import ms from "ms";
import { lighten } from "polished";
import { useMemo } from "react";
import { toSiacoins } from "sia-typescript/build/main/lib/currency";
import { jsx, Box, Styled } from "theme-ui";
import { useRenter } from "../api";
import { colors } from "../theme";

// make sure parent container have a defined height when using
// responsive component, otherwise height will be 0 and
// no chart will be rendered.
// website examples showcase many properties,
// you'll often use just a few of them.
export const SpendingPie = () => {
  const { data: renter } = useRenter({ refreshInterval: ms("5 seconds") });
  const pie = useMemo(() => {
    return [
      {
        id: "storage",
        label: "Storage Spend",
        value: renter ? toSiacoins(renter.financialmetrics.storagespending).toNumber() : 0,
      },
      {
        id: "upload",
        label: "Upload Spend",
        value: renter ? toSiacoins(renter.financialmetrics.uploadspending).toNumber() : 0,
      },
      {
        id: "download",
        label: "Download Spend",
        value: renter ? toSiacoins(renter.financialmetrics.downloadspending).toNumber() : 0,
      },
      {
        id: "unspent",
        label: "Remaining",
        value: renter ? toSiacoins(renter.financialmetrics.unspent).toNumber() : 0,
      },
    ];
  }, [renter]);

  const remainingSpend = pie.find(({ id }) => id === "unspent").value ?? 0;
  return (
    <Box sx={{ height: "100%", width: "100%", position: "relative" }}>
      <Box
        sx={{
          position: "absolute",
          left: 0,
          top: 0,
          height: "100%",
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Box sx={{ mb: "50px", textAlign: "center" }}>
          <Styled.p sx={{ lineHeight: 1, my: 0 }}>{remainingSpend.toFixed(2)} SC</Styled.p>
          <span
            sx={{
              fontSize: 0,
              textTransform: "uppercase",
              fontWeight: "bold",
              opacity: 0.4,
            }}
          >
            Remaining
          </span>
        </Box>
      </Box>
      <ResponsivePie
        data={pie}
        margin={{ top: 0, right: 0, bottom: 60, left: 0 }}
        innerRadius={0.5}
        startAngle={0}
        padAngle={0.7}
        cornerRadius={0}
        colors={(c) => {
          const map = {
            download: "#E7B448",
            upload: "#327572",
            storage: colors.primary,
            unspent: colors.background,
          };
          return map[c.id];
        }}
        borderWidth={1}
        borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
        enableRadialLabels={false}
        radialLabelsSkipAngle={10}
        radialLabelsTextXOffset={6}
        radialLabelsTextColor="#ffffff"
        radialLabelsLinkOffset={-24}
        radialLabelsLinkDiagonalLength={16}
        radialLabelsLinkHorizontalLength={24}
        radialLabelsLinkStrokeWidth={1}
        radialLabelsLinkColor={{ from: "color" }}
        radialLabel="label"
        sliceLabel="label"
        enableSlicesLabels={false}
        theme={{
          tooltip: {
            container: {
              backgroundColor: lighten(0.1, colors.card),
              borderRadius: 4,
            },
          },
          labels: {
            text: {
              fontWeight: 500,
            },
          },
        }}
        slicesLabelsSkipAngle={30}
        tooltip={(d) => (
          <div
            sx={{
              display: "flex",
              height: 40,
              color: "text",
              alignItems: "center",
            }}
          >
            <b sx={{ pr: 2 }}>{d.label}:</b> {d.value.toFixed(2)} SC
          </div>
        )}
        slicesLabelsTextColor={colors.text}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        defs={[
          {
            id: "dots",
            type: "patternDots",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            size: 4,
            padding: 1,
            stagger: true,
          },
          {
            id: "lines",
            type: "patternLines",
            background: "inherit",
            color: "rgba(255, 255, 255, 0.3)",
            rotation: -45,
            lineWidth: 6,
            spacing: 10,
          },
        ]}
        legends={[
          {
            anchor: "bottom",
            direction: "row",
            translateY: 56,
            itemWidth: 70,
            itemsSpacing: 30,
            itemHeight: 18,
            justify: true,
            itemTextColor: "#999",
            symbolSize: 18,
            symbolShape: "circle",
            effects: [
              {
                on: "hover",
                style: {
                  itemTextColor: "#fff",
                },
              },
            ],
          },
        ]}
      />
    </Box>
  );
};
