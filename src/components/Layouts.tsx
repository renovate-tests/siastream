/** @jsx jsx */
import { Box, Tooltip, IconButton } from "@material-ui/core";
import { PlayCircleOutline, PauseCircleOutline, HelpOutline, Receipt, InfoOutlined } from "@material-ui/icons";
import axios from "axios";
import { motion } from "framer-motion";
import { useSnackbar } from "notistack";
import { useState, useCallback, Suspense, useContext } from "react";
import { Container, Footer, Header, jsx, Layout, Main } from "theme-ui";
import SiaCoin from "../components/SiaCoin";
import { AnimFadeIn } from "../components/Transitions";
import { DispatchContext } from "../states/Context";
import { ContextMenu } from "./ContextMenu";
import LogDialog from "./LogDialog";
import SiadStatusChip from "./SiadStatusChip";

export const DashboardLayout = (p) => {
  const dispatch = useContext(DispatchContext);
  const [paused, setPaused] = useState(false);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [currentSnackbar, setCurrentSnackbar] = useState(null);

  const onRenterStateChange = useCallback(async () => {
    try {
      setPaused(!paused);
      await axios.get(`siad/renter/uploads/${paused ? "resume" : "pause"}`);

      closeSnackbar(currentSnackbar);

      setCurrentSnackbar(enqueueSnackbar(paused ? "Uploads resumed" : "Uploads paused for 2 hours"));
    } catch (error) {
      setPaused(paused); // revert the state on error
    }
  }, [closeSnackbar, currentSnackbar, setCurrentSnackbar, enqueueSnackbar, paused]);

  return (
    <Layout>
      <Header>
        <Container>
          <motion.div {...AnimFadeIn}>
            <Box display="flex" justifyContent="space-between" alignItems="center">
              <SiaCoin />

              <Box display="flex" alignItems="center">
                <Suspense fallback={null}>
                  <Box mr={2}>
                    <SiadStatusChip />
                  </Box>
                </Suspense>

                <Tooltip
                  title={paused ? "Uploads paused, click to resume" : "Pause uploads for 2 hours"}
                  aria-label={paused ? "resume" : "pause"}
                >
                  <IconButton onClick={onRenterStateChange}>
                    {paused ? <PauseCircleOutline /> : <PlayCircleOutline />}
                  </IconButton>
                </Tooltip>

                <Tooltip title={"Show log"} aria-label="show log">
                  <IconButton onClick={() => dispatch({ type: "LOG/OPEN" })}>
                    <Receipt />
                  </IconButton>
                </Tooltip>

                <Tooltip title={"Support site"} aria-label="support">
                  <IconButton href="https://support.siastream.tech" target="_blank">
                    <HelpOutline />
                  </IconButton>
                </Tooltip>

                <Tooltip title={"Sia status"} aria-label="status">
                  <IconButton onClick={() => dispatch({ type: "SIDEBAR/OPEN" })}>
                    <InfoOutlined />
                  </IconButton>
                </Tooltip>
              </Box>
            </Box>
          </motion.div>
        </Container>
      </Header>
      <Main>
        <Container {...p} />
        <ContextMenu />
        <LogDialog />
      </Main>
      <Footer>
        <Container>
          <a href="https://sia.tech/" target="_blank" rel="noopener noreferrer">
            <Box
              sx={{
                textAlign: "center",
              }}
            >
              <img sx={{ width: 80, height: "100%" }} src="/images/built-with-sia.png" alt="Built with Sia" />
            </Box>
          </a>
        </Container>
      </Footer>
    </Layout>
  );
};
