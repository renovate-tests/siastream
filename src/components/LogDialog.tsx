import { Dialog, Button, Link, DialogContent, DialogActions, CircularProgress } from "@material-ui/core";
import axios from "axios";
import { useSnackbar } from "notistack";
import React, { useContext, useState } from "react";
import { LazyLog } from "react-lazylog";
import { getUrl } from "skynet-js";
import { DispatchContext, StateContext } from "../states/Context";

export default function LogDialog() {
  const state = useContext(StateContext);
  const dispatch = useContext(DispatchContext);
  const [sharing, setSharing] = useState(false);
  const [shareUrl, setShareUrl] = useState("");
  const { enqueueSnackbar } = useSnackbar();
  const handleClose = () => {
    setShareUrl(""); // reset url
    dispatch({ type: "LOG/CLOSE" });
  };

  const handleShare = async () => {
    setSharing(true);

    try {
      const { data } = await axios.post("/api/log/share");

      setShareUrl(getUrl("https://siasky.net", data.skylink));
    } catch (error) {
      const message = error?.response?.data?.message ?? error.message;

      enqueueSnackbar(`SiaStream failed to upload log to Skynet: ${message}`, { variant: "error" });
    }

    setSharing(false);
  };

  return (
    <Dialog open={state.logOpened} onClose={handleClose} fullScreen>
      <DialogContent dividers={true} style={{ padding: "2px" }}>
        <LazyLog url="/api/log/download" follow selectableLines={true} />
      </DialogContent>
      <DialogActions>
        {shareUrl && !sharing && (
          <Link component={Button} href={shareUrl} target="_blank" rel="noopener noreferrer">
            {shareUrl}
          </Link>
        )}
        {sharing && <CircularProgress size={16} />}
        <Button onClick={handleShare} color="primary" disabled={sharing || Boolean(shareUrl)}>
          Share via Skynet
        </Button>
        <Button onClick={handleClose} color="primary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
