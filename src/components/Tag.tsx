/** @jsx jsx */
import { Box, jsx } from "theme-ui";

export const Tag = ({ color = "card", variant = "normal", ...p }) => (
  <Box
    sx={{
      borderRadius: 2,
      bg: color,
      textAlign: "center",
      variant: `tags.${variant}`,
    }}
    {...p}
  />
);

export const ActiveTag = ({ active = false, ...p }) => {
  return <Tag color={active ? "card" : "transparent"} {...p} />;
};
