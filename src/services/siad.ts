import { floor } from "lodash";
import { ConsensusGET } from "sia-typescript";

/**
 * Calculates estimated consensus sync progress based on the genesis timestamp and block frequency.
 * Returns progress value between 0 and 100.
 */
export function calculateConsensusSyncProgress(consensus: ConsensusGET) {
  if (!consensus?.height) return 0;

  const currentTimestamp = Date.now() / 1000; // divide by 1000 for milliseconds
  const estimatedHeight = (currentTimestamp - consensus.genesistimestamp) / consensus.blockfrequency;
  const progress = (consensus.height / estimatedHeight) * 100;

  return progress < 100 ? floor(progress, 1) : 99.9; // don't show 100 or more if not done
}
