import { StringSchema } from "yup";
declare module "yup" {
  interface StringSchema {
    validDir(message?: string): StringSchema;
    validBaseDir(message?: string): StringSchema;
    validSiaPath(message?: string): StringSchema;
  }
}
