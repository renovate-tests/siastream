import {
  Card,
  CardHeader,
  CardActions,
  CardContent,
  Typography,
  Button,
  Box,
  Grid,
  Chip,
  Fab,
} from "@material-ui/core";
import { NavigateNext, NavigateBefore } from "@material-ui/icons";
import { ToggleButtonGroup, ToggleButton } from "@material-ui/lab";
import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { StateContext } from "../../../states/Context";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";
import useCurrentStepInit from "../hooks/useCurrentStepInit";

const currentStep = {
  help: {
    text: "What is Seed?",
    href: "https://support.siastream.tech/article/wy1c7jv279-siastream-seed",
  },
};

enum View {
  Single = "single", // single word displayed, one by one
  All = "all", // all words displayed at the same time
}

export default function WalletInitSeed() {
  const history = useHistory();
  const state = useContext(StateContext);
  const seed = state.setup.seed;
  const seedWords = seed.split(" "); // seed consists of 28 or 29 words separated by spaces
  const [currentWordIndex, setCurrentWordIndex] = useState(0);
  const [view, setView] = useState(View.Single);
  const handleViewChange = (event: React.MouseEvent, value: View) => setView(value);
  const handlePreviousWord = () => setCurrentWordIndex(currentWordIndex - 1);
  const handleNextWord = () => setCurrentWordIndex(currentWordIndex + 1);
  const currentText = view === View.Single ? seedWords[currentWordIndex] : seed;
  const handleCopy = () => navigator.clipboard.writeText(currentText);
  const handleNext = () => history.push("/setup/media");

  useCurrentStepInit(currentStep);

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="Save your seed" titleTypographyProps={{ variant: "h2", align: "center" }} />
        <CardContent>
          <Box display="flex" justifyContent="center" mb={6}>
            <ToggleButtonGroup
              value={view}
              exclusive
              onChange={handleViewChange}
              aria-label="seed view toggle"
              size="small"
            >
              <ToggleButton value={View.Single}>One By One</ToggleButton>
              <ToggleButton value={View.All}>Show All</ToggleButton>
            </ToggleButtonGroup>
          </Box>

          <Grid container justify="center">
            <Grid item xs={9}>
              <Card variant="outlined" raised>
                <CardContent>
                  <Box m={4}>
                    <Typography variant={view === "single" ? "h2" : "h3"} align="center">
                      {currentText}
                    </Typography>
                  </Box>
                </CardContent>
                <CardActions>
                  <Box display="flex" justifyContent="center" width="100%">
                    <Button size="small" onClick={handleCopy}>
                      Copy to clipboard
                    </Button>
                  </Box>
                </CardActions>
              </Card>
            </Grid>

            {view === "single" && (
              <Grid item xs={9}>
                <Navigation alignItems="center">
                  <Fab aria-label="previous seed word" onClick={handlePreviousWord} disabled={!currentWordIndex}>
                    <NavigateBefore />
                  </Fab>
                  <Chip variant="outlined" label={`${currentWordIndex + 1} / ${seedWords.length}`} />
                  <Fab
                    color="primary"
                    aria-label="next seed word"
                    onClick={handleNextWord}
                    disabled={currentWordIndex === seedWords.length - 1}
                  >
                    <NavigateNext />
                  </Fab>
                </Navigation>
              </Grid>
            )}
          </Grid>
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <NextButton disabled={view === "single" && currentWordIndex < seedWords.length - 1} onClick={handleNext} />
      </Navigation>
    </StepTransitionAnimation>
  );
}
