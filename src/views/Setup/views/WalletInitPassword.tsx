import { Card, CardHeader, CardContent, FormControlLabel, Checkbox, TextField, Box } from "@material-ui/core";
import retry from "async-retry";
import axios from "axios";
import { useSnackbar } from "notistack";
import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { useSiaStreamSettings, useWallet } from "../../../api";
import { DispatchContext } from "../../../states/Context";
import { useDBConfig } from "../../../states/DBConfig";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function WalletInitPassword() {
  const { data: wallet, mutate: mutateWallet } = useWallet({ suspense: true });
  const { enqueueSnackbar } = useSnackbar();
  const { mutate: mutateStreamConfig } = useSiaStreamSettings("stream");
  const [autounlock, setAutounlock] = useState(!wallet.unlocked);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] = useState("");
  const { setStreamConfig } = useDBConfig();
  const dispatch = useContext(DispatchContext);
  const history = useHistory();
  const handleNext = async () => {
    setPasswordErrorMessage("");
    setConfirmPasswordErrorMessage("");

    if (!password) {
      setPasswordErrorMessage("Password cannot be empty");
      return;
    }

    if (password !== confirmPassword) {
      setConfirmPasswordErrorMessage("Passwords do not match");
      return;
    }

    try {
      const { data } = await axios.post("/siad/wallet/init", { encryptionpassword: password, force: wallet.encrypted });

      // always unlock the wallet initially in process of setting up new wallet
      await retry(() => axios.post("/siad/wallet/unlock", { encryptionpassword: password }));

      dispatch({ type: "SETUP/SEED", payload: { seed: data.primaryseed } });
    } catch (error) {
      const message = error.response ? error.response.data.message : error.message;

      enqueueSnackbar(message, { variant: "error" });
      return;
    }

    await mutateStreamConfig(setStreamConfig({ password: autounlock ? password : null }));

    mutateWallet(); // refresh wallet

    history.push("/setup/wallet/init/seedintro");
  };
  const handleAutounlockChange = () => setAutounlock((autounlock) => !autounlock);
  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
    setPasswordErrorMessage(""); // reset password error on password change
  };
  const handleConfirmPasswordChange = (event) => {
    setConfirmPassword(event.target.value);
    setConfirmPasswordErrorMessage(""); // reset password error on password change
  };

  return (
    <StepTransitionAnimation>
      <Card>
        <CardHeader title="Secure SiaStream with a password" titleTypographyProps={{ variant: "h2" }} />
        <CardContent>
          <Box mb={3}>
            <TextField
              label="Password"
              type="password"
              fullWidth
              autoFocus
              error={Boolean(passwordErrorMessage)}
              helperText={passwordErrorMessage}
              autoComplete="new-password"
              onChange={handlePasswordChange}
            />
          </Box>

          <Box mb={3}>
            <TextField
              label="Confirm password"
              type="password"
              fullWidth
              error={Boolean(confirmPasswordErrorMessage)}
              helperText={confirmPasswordErrorMessage}
              autoComplete="new-password"
              onChange={handleConfirmPasswordChange}
            />
          </Box>

          <Box mb={3}>
            <FormControlLabel
              control={
                <Checkbox checked={autounlock} name="autounlock" onChange={handleAutounlockChange} color="primary" />
              }
              label="SiaStream requires the wallet to be unlocked at all times to operate. Allow SiaStream to manage this for you."
            />
          </Box>
        </CardContent>
      </Card>

      <Navigation>
        <BackButton />

        <NextButton onClick={handleNext} />
      </Navigation>
    </StepTransitionAnimation>
  );
}
