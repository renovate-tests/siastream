import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Box,
  TextField,
  FormControlLabel,
  Checkbox,
  Divider,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import axios from "axios";
import { useFormik } from "formik";
import ms from "ms";
import React from "react";
import { useHistory } from "react-router-dom";
import * as Yup from "yup";
import { useSiaStreamSettings, useConsensus, useWallet } from "../../../api";
import { useDBConfig } from "../../../states/DBConfig";
import BackButton from "../components/BackButton";
import Navigation from "../components/Navigation";
import NextButton from "../components/NextButton";
import StepTransitionAnimation from "../components/StepTransitionAnimation";

export default function WalletRestore() {
  const { mutate: mutateStreamConfig } = useSiaStreamSettings("stream");
  const { data: consensus } = useConsensus({ suspense: true, refreshInterval: ms("5 seconds") });
  const { data: wallet, mutate: mutateWallet } = useWallet({ suspense: true, refreshInterval: ms("5 seconds") });
  const history = useHistory();
  const { setStreamConfig } = useDBConfig();
  const formik = useFormik({
    initialValues: {
      seed: "",
      password: "",
      confirmPassword: "",
      autounlock: true,
    },
    validationSchema: Yup.object({
      seed: Yup.string().required("Seed cannot be empty"),
      password: Yup.string().required("Password cannot be empty"),
      confirmPassword: Yup.string()
        .required("Password cannot be empty")
        .oneOf([Yup.ref("password")], "Passwords must match"),
      autounlock: Yup.boolean(),
    }),
    onSubmit: async ({ seed, password, autounlock }, { setSubmitting, setFieldError, resetForm }) => {
      try {
        /**
         * Restoring from seed is a long running, blocking request. It may take several minutes but
         * if the process would fail (due to invalid seed for example), it would fail almost instantly.
         * Because of that it is safe to assume that if the init request takes more than around 2s, then
         * it will succeed at some point and there is no reason to wait for it.
         */
        const init = axios.post("/siad/wallet/init/seed", {
          seed,
          encryptionpassword: password,
          force: wallet.encrypted,
        });
        const timer = new Promise((resolve) => setTimeout(resolve, ms("2 seconds")));

        // if init doesn't fail fast then it means it started rescanning
        await Promise.race([init, timer]);
      } catch (error) {
        mutateWallet(); // refresh wallet on error
        setFieldError("seed", error.response ? error.response.data.message : error.message);

        return;
      }

      // save password or set it to blank if user decided not to allow autounlock
      await mutateStreamConfig(setStreamConfig({ password: autounlock ? password : null }));

      mutateWallet(); // refresh wallet

      resetForm();
      setSubmitting(false);

      history.push("/setup/media");
    },
  });

  return (
    <StepTransitionAnimation>
      <form onSubmit={formik.handleSubmit}>
        <Card>
          <CardHeader title="Restore from seed" titleTypographyProps={{ variant: "h2" }} />
          <CardContent>
            <Typography paragraph>Enter your existing seed and set new password to secure SiaStream.</Typography>

            <Box mb={3}>
              <TextField
                label="Seed"
                name="seed"
                fullWidth
                multiline
                autoFocus
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.seed && Boolean(formik.errors.seed)}
                helperText={formik.touched.seed && formik.errors.seed}
                value={formik.values.seed}
              />
            </Box>

            <Box mb={3} mt={3}>
              <Divider />
            </Box>

            <Typography paragraph>Secure SiaStream with a password</Typography>

            <Box mb={3}>
              <TextField
                label="Enter password"
                type="password"
                name="password"
                autoComplete="new-password"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                value={formik.values.password}
              />
            </Box>

            <Box mb={3}>
              <TextField
                label="Confirm password"
                type="password"
                name="confirmPassword"
                autoComplete="new-password"
                fullWidth
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                value={formik.values.confirmPassword}
              />
            </Box>

            <Box mb={3}>
              <FormControlLabel
                control={
                  <Checkbox
                    color="primary"
                    name="autounlock"
                    checked={formik.values.autounlock}
                    onChange={formik.handleChange}
                  />
                }
                label="SiaStream requires the wallet to be unlocked at all times to operate. Allow SiaStream to manage this for you."
              />
            </Box>
          </CardContent>
        </Card>

        {!consensus.synced && (
          <Box mt={4}>
            <Alert severity="warning">
              SiaStream must be 100% synced in order to restore your wallet. Sync status is displayed in the upper-right
              corner.
            </Alert>
          </Box>
        )}

        <Navigation>
          <BackButton />

          <NextButton disabled={!consensus.synced || formik.isSubmitting} loading={formik.isSubmitting} />
        </Navigation>
      </form>
    </StepTransitionAnimation>
  );
}
