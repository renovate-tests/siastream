import React, { createContext, useReducer } from "react";

export const StateContext = createContext(null);
export const DispatchContext = createContext(null);

type State = {
  setup: {
    complete: boolean;
    initialized: boolean;
    currentStep: object;
  };
  sidebarOpened: boolean;
  logOpened: boolean;
  depositDialogOpened: boolean;
};
type Action = { type: string; payload: any };

export const initialState: State = {
  setup: {
    complete: false,
    initialized: false,
    currentStep: {},
  },
  sidebarOpened: false,
  logOpened: false,
  depositDialogOpened: false,
};

export function reducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case "SETUP/INITIALIZE":
      return { ...state, setup: { ...state.setup, initialized: true } };
    case "SETUP/SEED":
      return { ...state, setup: { ...state.setup, seed: action.payload.seed } };
    case "SETUP/STEP/ENTER":
      return { ...state, setup: { ...state.setup, currentStep: { ...state.setup.currentStep, ...action.payload } } };
    case "SETUP/STEP/EXIT":
      return { ...state, setup: { ...state.setup, currentStep: {} } };
    case "SIDEBAR/OPEN":
      return { ...state, sidebarOpened: true };
    case "SIDEBAR/CLOSE":
      return { ...state, sidebarOpened: false };
    case "LOG/OPEN":
      return { ...state, logOpened: true };
    case "LOG/CLOSE":
      return { ...state, logOpened: false };
    case "DEPOSIT_DIALOG/OPEN":
      return { ...state, depositDialogOpened: true };
    case "DEPOSIT_DIALOG/CLOSE":
      return { ...state, depositDialogOpened: false };
    default:
      return state;
  }
}

export default function Context({ children }: { children: React.ReactNode }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <DispatchContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>{children}</StateContext.Provider>
    </DispatchContext.Provider>
  );
}
