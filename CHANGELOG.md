## Version Scheme

SiaStream uses the following versioning scheme, vX.X.X

- First Digit signifies a major (compatibility breaking) release
- Second Digit signifies a major (non compatibility breaking) release
- Third Digit signifies a minor or patch release

## Version History

Latest:

### v0.1.0
**Key Updates**
- onboarding went through complete revamp (it is now possible to run the setup
  again by accessing /setup route) - improved CLI for production builds
  `siastream --help` - updated minimal version of Sia to 1.4.8
- BREAKING CHANGE: if you changed the path where siastream keeps your data in
  sia from /siastream, you will need to manually change it again, othewise it
  will default to /siastream
- Significant application architecture rewrite
  [#37](https://gitlab.com/NebulousLabs/siastream/-/merge_requests/37)

**Bugs Fixed**
- fixed an issue with siastream trying to mount a fuse directory on a path that
  is already mounted - happened when siad did not unmount the directory
  properly, ie. when the process crashed
- SiaStream will now wait for renter to be ready before trying to upload files -
  SiaStream will wait on specific siad modules to be operational before using
  them
- ensure that fuse mountpoint exists before mounting fuse - fixes issue where if
  user toggled fuse from the webapp too fast, it tried to mount it twice

**Other**
- Add script to install prerequisities (node, yarn, FUSE) on MacOS.
- Add script and readme how to setup MacOS Gitlab runner.
- included various help messages, tooltips and support articles to improve user
  experience
- improved onboarding seed info screen to explicitly require user confirmation
- use /siastream for data directory in development mode
- added eslint as static code analysis to ensure code quality
- ensure support for Node.js 14
- added new Sia daemon status chip in top right corner to inform about consensus
  syncing, wallet rescanning or service interruptions - switched from nexe to
  pkg as a main binary bundler which allowed us to update bundled node version
  to 12.16.x - decreased level of logging for very rarely used messages
  (`silly`), you can set log level with `LOG_LEVEL` env variable (available
  levels: https://github.com/winstonjs/winston#logging)
  