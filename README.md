# SiaStream

An easy to use interface to manage your Plex library on Sia.

#### About Sia

Sia is a decentralized storage network that allows users to rent out cloud storage at an ultra competitive price. As a permissionless network and open marketplace, it offers some key advantages that makes it a compelling offering to home media server operators.

#### Cheaper Storage

The price to store a single TB on Amazon S3 or Google's Cloud Platform is around \$20/TB. On Sia, storage providers (hosts) are able to set their own prices, while storage users (renters) each individually select the hosts they choose to store their files with.

The current cost of storing 1 TB of data on the Sia network is $0.89 (as of Nov 26 2019). Even considering a 3x data redundancy (to account for storage providers that go offline), the cost per TB on the Sia network is only around $2-3/TB.

#### No Deplatforming

Any experienced Plex user that has tried hosting their data on Amazon Drive Unlimited or Google Drive Unlimited know that both companies have since changed their policies to remove their unlimited offerings. Users have even been banned by centralized cloud-storage companies due to violations in their "policy".

Since Sia is fully decentralized, it means no single authority can deny you access to storing and streaming files from the Sia platform. In fact, even if Sia's parent company shuttered its doors today, the network would continue to function normally.

## Usage

### Prerequisites

- required: [node](https://nodejs.org) (preferably version 12+)
  - macos: `brew install node` with [brew](https://brew.sh/)
  - linux: [node installation docs](https://nodejs.org/en/download/package-manager/)
- required: [yarn](https://yarnpkg.com)
  - macos: `brew install yarn` with [brew](https://brew.sh/)
  - linux: [yarn installation docs](https://classic.yarnpkg.com/en/docs/install/)

#### FUSE

SiaStream uses FUSE to mount uploaded data from Sia network to your local directory. Depending on your setup you might need to:

- macos only: [FUSE for mac](https://osxfuse.github.io/)
- linux only: if you're running as a non root user, you need to edit `/etc/fuse.conf` and include `user_allow_other` directive - [example](https://askubuntu.com/a/309242)

### Starting development build

- make sure that you meet all the [prerequisites](#prerequisites)
- `yarn` - installs dependencies
- `yarn start` - starts app on localhost:3000

### Building distribution package

- make sure that you meet all the [prerequisites](#prerequisites)
- `yarn` - installs dependencies
- depending on your target environment
  - `yarn package linux` - for linux
  - `yarn package macos` - for macos
- package will be available in `dist` directory under a directory specific to the target platform ex. `dist/siastream-macos-x64` and will contain:
  - `siastream` file - application executable
  - `node_modules` directory - has to be shipped alongside the executable (contains native addons and libraries that could not be bundled in the executable)
