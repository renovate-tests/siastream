import retry from "async-retry";
import axios from "axios";

type Currency = "usd"; // currently only supported currency is usd

// Pulls Siacoin to USD exchange rate.
export const getSiacoinExchangeRate = async (currency: Currency = "usd") => {
  const { data } = await retry(() =>
    axios(`https://api.coingecko.com/api/v3/simple/price?ids=siacoin&vs_currencies=${currency}`)
  );

  return data.siacoin[currency] as number;
};
