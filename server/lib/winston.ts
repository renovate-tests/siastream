import path from "path";
import stringify from "json-stringify-safe";
import { isEmpty } from "lodash";
import winston from "winston";
import { SIASTREAM_LOG_DIR } from "./paths";

// define a level that is at least "debug" but if it is overridden by user it can be even more verbose
const atLeastLogLevel = (level: string) => {
  const userDefinedPriority = winston.config.npm.levels[process.env.SIASTREAM_LOG_LEVEL as string];
  const levelPriority = winston.config.npm.levels[level];

  return userDefinedPriority < levelPriority ? level : process.env.SIASTREAM_LOG_LEVEL;
};

const timestamp = winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss" });
const printf = winston.format.printf(({ timestamp, level, label, message, ...rest }) => {
  const metadata = !isEmpty(rest) && stringify(rest); // safely stringify rest of the metadata

  return [timestamp, level, label && `(${label})`, message, metadata].filter(Boolean).join(" ");
});

const logger = winston.createLogger({
  transports: [
    // file logger should log everything from debug at least for the beta version
    new winston.transports.File({
      level: atLeastLogLevel("debug"),
      filename: path.resolve(SIASTREAM_LOG_DIR, "siastream.log"),
      format: winston.format.combine(timestamp, printf),
    }),
    // console logger logs info and above
    new winston.transports.Console({
      level: process.env.SIASTREAM_LOG_LEVEL || "info",
      format: winston.format.combine(winston.format.colorize(), timestamp, printf),
    }),
  ],
});

logger.info(`Logs persisting to ${SIASTREAM_LOG_DIR}`);

export default logger;
