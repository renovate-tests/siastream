import os from "os";
import path from "path";
import { UploadFile } from "@nebulous/skynet";
import { Router } from "express";
import { LowdbSync } from "lowdb";
import publicIp from "public-ip"; // eslint-disable-line import/default
import db from "../db";
import { SIASTREAM_LOG_DIR } from "../lib/paths";
import { invokeJobs } from "../scheduler";
import { getSiaClient } from "../siaClient";
import datamap from "../utils/datamap";
import { asyncMiddleware } from "./middleware";

const router = Router();

router.get("/healthcheck", (_, res) => {
  res.status(200).send({ status: "OK" });
});

router.get("/os", (_, res) => {
  res.status(200).send({ platform: os.platform() });
});

function createPartialSetHandler<T extends {}>(dbSyncAdapter: LowdbSync<T>) {
  return asyncMiddleware(async (req, res) => {
    const partialConfig: Partial<T> = req.body;
    const result = db.partialSet(dbSyncAdapter, partialConfig);

    invokeJobs(); // invoke all scheduler jobs on settings update

    res.status(200).send(result);
  });
}

function getStateHandler<T>(dbSyncAdapter: LowdbSync<T>) {
  return asyncMiddleware(async (_, res) => {
    const config = dbSyncAdapter.getState();
    res.status(200).send(config);
  });
}

router.post("/config/allowance", createPartialSetHandler(db.allowance));
router.get("/config/allowance", getStateHandler(db.allowance));

router.get("/config/siad", getStateHandler(db.siad));

router.get("/config/stream", getStateHandler(db.stream));
router.post("/config/stream", createPartialSetHandler(db.stream));

router.get(
  "/geolocation",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const { activecontracts, passivecontracts } = await SiaClient.GetContracts();
    const sourceip = await publicIp.v4();
    const contracts = [...(activecontracts || []), ...(passivecontracts || [])];
    const result = await Promise.all(datamap(sourceip, contracts));
    res.status(200).send(result);
  })
);

router.post(
  "/log/share",
  asyncMiddleware(async (req, res) => {
    const skylink = await UploadFile(path.resolve(SIASTREAM_LOG_DIR, "siastream.log"), {
      customFilename: "siastream.log",
      portalFileFieldname: "file",
      portalUrl: "https://siasky.net",
      portalUploadPath: "/skynet/skyfile",
    });

    res.status(200).send({ skylink });
  })
);

router.get(
  "/log/download",
  asyncMiddleware(async (req, res) => {
    res.download(path.resolve(SIASTREAM_LOG_DIR, "siastream.log"));
  })
);

export default router;
