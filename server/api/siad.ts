import expandTilde from "expand-tilde";
import { Router } from "express";
import ms from "ms";
import db from "../db";
import { getSiaClient } from "../siaClient";
import { asyncMiddleware } from "./middleware";
import fuseMount from "./siad/fuseMount";

const router = Router();

router.get(
  "/healthcheck",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const running = await SiaClient.isRunning();
    res.send({ running });
  })
);

router.get(
  "/fuse",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.FuseSettings();
    res.send(d);
  })
);

router.post(
  "/fuse/mount",
  asyncMiddleware(async (req, res) => {
    const d = await fuseMount();
    res.status(200).send(d);
  })
);

router.post(
  "/fuse/unmount",
  asyncMiddleware(async (req, res) => {
    const config = db.stream.getState();
    const pathToUnmount = expandTilde(config.fuseMountPath);
    const SiaClient = getSiaClient();
    const d = await SiaClient.UnmountFuse(pathToUnmount);
    res.status(200).send(d);
  })
);

router.get(
  "/consensus",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.Consensus();
    res.send(d);
  })
);

router.get(
  "/gateway",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.Gateway();
    res.send(d);
  })
);

router.get(
  "/version",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.DaemonVersion();
    res.send(d);
  })
);

router.get(
  "/wallet",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.Wallet();
    res.send(d);
  })
);

router.post(
  "/wallet/init",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword, force } = req.body;
    const SiaClient = getSiaClient();
    const d = await SiaClient.InitWallet(encryptionpassword, "english", force);
    res.send(d);
  })
);

router.post(
  "/wallet/unlock",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword } = req.body;
    const SiaClient = getSiaClient();
    const d = await SiaClient.UnlockWallet(encryptionpassword);
    res.send(d);
  })
);

router.get(
  "/wallet/verifypassword",
  asyncMiddleware(async (req, res) => {
    const password = req.query.password;
    const SiaClient = getSiaClient();
    const d = await SiaClient.VerifyWalletPassword(password);
    res.send(d);
  })
);

router.post(
  "/wallet/changepassword",
  asyncMiddleware(async (req, res) => {
    const { encryptionpassword, newpassword } = req.body;
    const SiaClient = getSiaClient();
    const d = await SiaClient.ChangePassword(encryptionpassword, newpassword);
    res.send(d);
  })
);

router.get(
  "/wallet/addresses",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.GetOrderedAddresses(5);
    res.send(d);
  })
);

router.get(
  "/wallet/address",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.GetAddress();
    res.send(d);
  })
);

router.get(
  "/renter",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.Renter();
    res.send(d);
  })
);

router.get(
  "/renter/contracts",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.GetContracts();
    res.send(d);
  })
);

router.get(
  "/wallet/seeds",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.makeRequest("/wallet/seeds");
    res.status(200).send(d);
  })
);

router.post(
  "/wallet/init/seed",
  asyncMiddleware(async (req, res) => {
    const { seed, encryptionpassword, force } = req.body;
    const SiaClient = getSiaClient();
    const d = await SiaClient.RestoreWallet(seed, { encryptionpassword, force });
    res.status(200).send(d);
  })
);

router.get(
  "/renter/recoveryscan",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.RecoveryScanProgress();
    res.status(200).send(d);
  })
);

router.get(
  "/daemon/alerts",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.makeRequest("/daemon/alerts");
    res.status(200).send(d);
  })
);

router.get(
  "/daemon/version",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.makeRequest("/daemon/version");
    res.status(200).send(d);
  })
);

router.get(
  "/daemon/update",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.makeRequest("/daemon/update");
    res.status(200).send(d);
  })
);

router.post(
  "/daemon/update",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const timeout = ms("5 minutes"); // extended timeout since it can take a while to update
    const d = await SiaClient.makeRequest("/daemon/update", undefined, "POST", timeout);
    res.status(200).send(d);
  })
);

router.get(
  "/daemon/constants",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.Constants();
    res.status(200).send(d);
  })
);

router.get(
  "/renter/files",
  asyncMiddleware(async (req, res) => {
    const SiaClient = getSiaClient();
    const d = await SiaClient.GetFiles();
    res.status(200).send(d);
  })
);

router.get(
  "/renter/uploads/pause",
  asyncMiddleware(async (_, res) => {
    const SiaClient = getSiaClient();
    await SiaClient.PauseUploads(2 * 60 * 60); // pause for 2 hours
    res.sendStatus(204);
  })
);

router.get(
  "/renter/uploads/resume",
  asyncMiddleware(async (_, res) => {
    const SiaClient = getSiaClient();
    await SiaClient.ResumeUploads();
    res.sendStatus(204);
  })
);

export default router;
