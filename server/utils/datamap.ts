import dns from "dns";
import geoip from "geoip-lite";
import isIP from "validator/lib/isIP";

/*

Goal:
The goal of the data object is to succinctly map every arc from the host back
to a single source ip.

map: srcposition [long, lat] <-> destposition [long, lat]
host details: IP / Pubkey

*/
export default function datamap(sourceIP: string, data: Array<any>): Array<Promise<any>> {
  const sourceIPGeodata = geoip.lookup(sourceIP);
  const sourceIPLocation = [sourceIPGeodata.ll[1], sourceIPGeodata.ll[0]];

  return data.map(async (m: any) => {
    const computeAndReturn = (targetIP: string) => {
      const targetIPGeodata = geoip.lookup(targetIP);
      const targetIPLocation = [targetIPGeodata.ll[1], targetIPGeodata.ll[0]];

      return {
        source: sourceIPLocation,
        target: targetIPLocation,
        netaddress: m.netaddress,
      };
    };
    const addr = m.netaddress.split(":")[0];

    if (isIP(addr, "4")) {
      return computeAndReturn(addr);
    } else {
      return await new Promise((resolve) => {
        dns.lookup(addr, (err, result) => {
          if (err) {
            resolve({
              source: sourceIPLocation,
              target: sourceIPLocation,
              netaddress: m.netaddress,
            });
          }
          return resolve(computeAndReturn(result));
        });
      });
    }
  });
}
