/* eslint-disable @typescript-eslint/no-unused-expressions */
import path from "path";
import expandTilde from "expand-tilde";
import yargs from "yargs";

export type SiaStreamArgv = {
  siastreamDir: string;
  siadDir: string;
  logDir: string;
  logLevel: "silly" | "debug" | "info" | "warn" | "error";
  host: string;
  port: number;
};

export const DEFAUlT_SIASTREAM_DATA_DIR = path.resolve(expandTilde("~"), "siastream");
export const DEFAUlT_SIAD_DATA_DIR = path.resolve(DEFAUlT_SIASTREAM_DATA_DIR, "sia");
export const DEFAUlT_SIASTREAM_LOG_DIR = path.resolve(DEFAUlT_SIASTREAM_DATA_DIR, "log");

yargs
  .command({
    command: "$0",
    describe: "Start Sia Stream",
    handler: async (argv: SiaStreamArgv) => {
      process.env.SIASTREAM_DATA_DIR = path.resolve(process.cwd(), expandTilde(argv.siastreamDir));
      process.env.SIAD_DATA_DIR = path.resolve(process.cwd(), expandTilde(argv.siadDir));
      process.env.SIASTREAM_LOG_DIR = path.resolve(process.cwd(), expandTilde(argv.logDir));
      process.env.SIASTREAM_LOG_LEVEL = argv.logLevel;

      require("./index")(argv);
    },
  })
  .option("siastream-dir", {
    describe: "SiaStream application local data directory",
    default: DEFAUlT_SIASTREAM_DATA_DIR,
    type: "string",
  })
  .option("siad-dir", {
    describe: "Sia daemon directory when running daemon internally",
    default: DEFAUlT_SIAD_DATA_DIR,
    type: "string",
  })
  .option("log-dir", {
    describe: "Application log directory",
    default: DEFAUlT_SIASTREAM_LOG_DIR,
    type: "string",
  })
  .option("log-level", {
    describe: "Application log level",
    default: "info",
    choices: ["silly", "debug", "info", "warn", "error"],
    type: "string",
  })
  .option("host", {
    describe: "Web interface host (use 0.0.0.0 to enable remote access)",
    default: "localhost",
    type: "string",
  })
  .option("port", {
    describe: "Web interface port",
    default: process.env.NODE_ENV === "development" ? 3001 : 3000,
    type: "number",
  })
  .wrap(yargs.terminalWidth())
  .help().argv;
