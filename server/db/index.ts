import fs from "fs";
import os from "os";
import path from "path";
import retry from "async-retry";
import lowdb, { LowdbSync } from "lowdb";
import FileSync from "lowdb/adapters/FileSync";
import { BLOCKS_PER_MONTH, Allowance } from "sia-typescript";
import { ClientConfig } from "sia-typescript/build/main/lib/proto";
import { SIASTREAM_DATA_DIR, SIAD_DATA_DIR } from "../lib/paths";
import logger from "../lib/winston";
import { getSiaClient } from "../siaClient";

export interface StreamDBConfig {
  dbRootPath: string; // the root path for all db types
}

const getMediaDir = () => {
  switch (os.platform()) {
    case "darwin":
      return "Movies";
    case "linux":
      return "Videos";
    default:
      return "media";
  }
};

export type StreamConfig = {
  isComplete: boolean;
  password: string | null;
  automaticAllowance: boolean;
  mountFuse: boolean;
  fuseMountPath: string;
  localPath: string;
  siaStreamSiaPath: string;
  removeMediaAfterUpload: boolean;
  nextFeeHeight: number | null;
  paidFeeHeight: number | null;
  nextMonthlyDepositHeight: number | null;
  increasedEstimatedStorageAlert: boolean;
};

const initialStreamConfig: StreamConfig = {
  isComplete: false,
  password: null,
  automaticAllowance: true,
  mountFuse: true,
  fuseMountPath: path.join(os.homedir(), "siafuse"),
  localPath: path.join(os.homedir(), getMediaDir()),
  siaStreamSiaPath: "/siastream",
  removeMediaAfterUpload: false,
  nextFeeHeight: null,
  paidFeeHeight: null,
  nextMonthlyDepositHeight: null,
  increasedEstimatedStorageAlert: false,
};

const initialSiadConfig: Partial<ClientConfig> = {
  agent: "Sia-Agent",
  dataDirectory: SIAD_DATA_DIR,
  apiAuthentication: "auto",
  apiHost: "localhost",
  apiPort: 9980,
  modules: {
    consensus: true,
    explorer: false,
    feeManager: true,
    gateway: true,
    host: false,
    miner: false,
    renter: true,
    transactionPool: true,
    wallet: true,
  },
};

const initialAllowanceConfig: Partial<Allowance> = {
  period: BLOCKS_PER_MONTH * 1, // period should be 1 month
  renewwindow: BLOCKS_PER_MONTH * 2, // renew window should be 2 months
};

export class StreamDB {
  dbRootPath: string;

  public stream: LowdbSync<StreamConfig>;
  public siad: LowdbSync<ClientConfig>;
  public allowance: LowdbSync<Partial<Allowance>>;

  constructor(config: StreamDBConfig) {
    this.dbRootPath = config.dbRootPath;
    this.initializeDir();

    const streamConfigPath = path.join(this.dbRootPath, "stream_config.json"); // stream config
    const siadConfigPath = path.join(this.dbRootPath, "siad_connection.json"); // siad config
    const allowanceConfigPath = path.join(this.dbRootPath, "allowance.json"); // allowance config

    // create DB adapters
    this.stream = this.createAdapter(streamConfigPath);
    this.siad = this.createAdapter(siadConfigPath);
    this.allowance = this.createAdapter(allowanceConfigPath);
  }

  public partialSet<T extends object>(db: LowdbSync<T>, partialConfig: Partial<T>) {
    const state = db.getState();
    const mergedConfig: object = { ...state, ...partialConfig };
    db.setState(mergedConfig as T).write();
    return mergedConfig;
  }

  private createAdapter(filepath: string) {
    return lowdb(new FileSync(filepath));
  }

  public async initializeDefaults() {
    const SiaClient = getSiaClient();
    const constants = await retry(() => SiaClient.Constants());

    const defaultAllowance = { ...constants.defaultallowance, ...initialAllowanceConfig };

    this.stream.defaults(initialStreamConfig).write();
    this.siad.defaults(initialSiadConfig).write();
    this.allowance.defaults(defaultAllowance).write();
  }

  private initializeDir() {
    try {
      fs.statSync(this.dbRootPath);
    } catch (e) {
      logger.debug("siastream root directory does not exist... creating:", this.dbRootPath);

      fs.mkdirSync(this.dbRootPath, { recursive: true });
    }
  }
}

export default new StreamDB({
  dbRootPath: SIASTREAM_DATA_DIR,
});
