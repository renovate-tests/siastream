import { debounce } from "lodash";
import ms from "ms";
import schedule from "node-schedule";
import {
  addFilesForUpload,
  deleteUploadedFiles,
  unlockWallet,
  updateEstimatedStorage,
  updateRenterSettings,
  feeManager,
  fuse,
  initialMonthlyDepositAlertCofig,
} from "./jobs";
import winston from "./lib/winston";

const logger = winston.child({ label: "Scheduler" });
const jobs: schedule.Job[] = [];

export default async function scheduler() {
  await clearScheduler(); // make sure to clear all jobs and watchers before starting

  // start registering files for upload job (every 30 minutes)
  jobs.push(schedule.scheduleJob("*/30 * * * *", addFilesForUpload));

  // start delete uploaded files job (every 30 minutes)
  jobs.push(schedule.scheduleJob("*/30 * * * *", deleteUploadedFiles));

  // start the fuse healthcheck job (every 30 minutes)
  jobs.push(schedule.scheduleJob("*/30 * * * *", fuse));

  // start the set allowance job (once a day at 4 AM)
  jobs.push(schedule.scheduleJob("0 4 * * *", updateEstimatedStorage));

  // start the set allowance job (every 30 minutes)
  jobs.push(schedule.scheduleJob("*/30 * * * *", updateRenterSettings));

  // start the fee manager job (every 10 minutes - avarage block time)
  jobs.push(schedule.scheduleJob("*/10 * * * *", feeManager));

  // start initial monthly deposit alert config job (once a week)
  jobs.push(schedule.scheduleJob("0 0 * * 0", initialMonthlyDepositAlertCofig));

  // start wallet unlock job (every 30 seconds)
  jobs.push(schedule.scheduleJob("*/30 * * * * *", unlockWallet));

  // run all the jobs immediately once and continue as ascheduled
  invokeJobs();
}

async function clearScheduler() {
  // stop all jobs and clear the queue
  jobs.forEach((job) => job.cancel());
  jobs.length = 0;
}

// debounce 1s not to run this in excess
export const invokeJobs = debounce(async () => {
  logger.debug("Jobs manually invoked, running all jobs");

  jobs.forEach((job) => job.invoke());
}, ms("1 second"));
