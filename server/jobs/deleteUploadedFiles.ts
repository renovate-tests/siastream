import fs from "fs";
import path from "path";
import retry from "async-retry";
import { trimStart } from "lodash";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete } from "./conditions";

const logger = winston.child({ label: "files deletion job" });

export default async function deleteUploadedFiles() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  const streamConfig = db.stream.getState();

  if (!streamConfig.removeMediaAfterUpload) {
    logger.silly("streamConfig.removeMediaAfterUpload setting disabled, skipping");

    return;
  }

  const files = await getReadyToDeleteFiles();

  if (!files.length) {
    logger.silly("No files ready to be deleted, skipping");

    return;
  }

  logger.silly(`Found ${files.length} uploaded files that are redundant locally, proceeding`);

  for (const file of files) {
    const localpath = path.resolve(streamConfig.localPath, file.basepath);

    try {
      await fs.promises.unlink(localpath);
    } catch (error) {
      if (error.code === "ENOENT") {
        logger.silly(`Local file "${localpath}" not found, skipping`);
      } else {
        logger.error(`Deleting redundant local file "${localpath}" failed with code ${error.code}, skipping`);
      }

      continue;
    }

    logger.debug(`Redundant local file "${localpath}" deleted successfully`);
  }
}

/**
 * This function lists all the files from siastream path that reached min redundancy.
 * Returned array will contain files that assuming sia base dir is "/siastream":
 * [ { siapath: "siastream/movies/avengers.mkv", basepath: "movies/avengers.mkv" } ]
 */
export async function getReadyToDeleteFiles(minRedundancy: number = 2.5) {
  const SiaClient = getSiaClient();
  const streamConfig = db.stream.getState();
  const { files } = await retry(() => SiaClient.GetFiles());

  if (!files) {
    return [];
  }

  // siastream siapath will start with / while siapath in files meta does not contain it
  // so we need to strip it from the siapath to be able to compare them
  const siaStreamSiaPath = trimStart(streamConfig.siaStreamSiaPath, path.posix.sep);
  const validFiles = files.filter(({ siapath, redundancy }) => {
    const isSiaStreamFile = siapath.startsWith(siaStreamSiaPath);
    const isReadyToDelete = redundancy >= minRedundancy;

    return isSiaStreamFile && isReadyToDelete;
  });

  return validFiles.map((file) => ({
    siapath: file.siapath,
    basepath: path.relative(siaStreamSiaPath, file.siapath),
  }));
}
