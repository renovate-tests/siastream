import { Logger } from "winston";
import db from "../../db";

/**
 * Check if the SiaStream is configured to automatically manage allowance
 *
 * @param logger instance of winston logger
 */
export default function isAutomaticAllowance(logger: Logger) {
  const streamConfig = db.stream.getState(); // get local stream config

  if (!streamConfig.automaticAllowance) {
    logger.silly("SiaStream configured to manually manage allowance, skipping");
  }

  return streamConfig.automaticAllowance;
}
