import retry from "async-retry";
import filesize from "filesize";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete, isAutomaticAllowance, isConsensusSynced } from "./conditions";

const logger = winston.child({ label: "updated estimated storage job" });
const threshold = 0.9; // fraction of storage that when reached will trigger estimated storage adjustment
const multiplier = 1.2; // storage adjustment multiplier once threshold is reached

export default async function updateEstimatedStorage() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if the SiaStream is configured to automatically manage allowance
  if (!isAutomaticAllowance(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  const SiaClient = getSiaClient();
  const allowance = db.allowance.getState(); // get local renter allowance settings
  const streamConfig = db.stream.getState(); // get local stream config

  // get siastream directory size
  const siaStreamDir = await retry(() => SiaClient.GetDir(streamConfig.siaStreamSiaPath));
  const siaStreamDirSize = siaStreamDir.directories[0].aggregatesize;

  if (!allowance.expectedstorage) {
    logger.silly(`Estimated storage not yet set, skipping`);

    return;
  }

  if (siaStreamDirSize < threshold * allowance.expectedstorage) {
    logger.silly(`SiaStream directory size does not exceed estimated storage threshold, skipping`, {
      currentStorage: filesize(siaStreamDirSize, { base: 10 }),
      allowanceExpectedStorage: filesize(allowance.expectedstorage, { base: 10 }),
    });

    return;
  }

  const expectedStorage = allowance.expectedstorage * multiplier; // new adjusted estimated storage

  db.partialSet(db.allowance, { expectedstorage: expectedStorage });
  db.partialSet(db.stream, { increasedEstimatedStorageAlert: true });

  logger.debug(`Estimated storage updated successfully to ${filesize(expectedStorage, { base: 10 })}`);
}
