import path from "path";
import retry from "async-retry";
import chokidar from "chokidar";
import ms from "ms";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isConsensusSynced, isSetupComplete } from "./conditions";

const logger = winston.child({ label: "files watcher job" });
const state: { fswatcher: chokidar.FSWatcher | null; localWatchPath: string } = { fswatcher: null, localWatchPath: "" };

export default async function registerFilesWatcher() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  const SiaClient = getSiaClient();
  const wallet = await retry(() => SiaClient.Wallet()); // get current wallet state

  // check whether wallet has allready been encrypted
  if (!wallet.encrypted) {
    logger.silly("Wallet not yet encrypted, skipping");

    return close();
  }

  // check whether wallet is already unlocked
  if (!wallet.unlocked) {
    logger.silly("Wallet not unlocked, skipping");

    return close();
  }

  // check whether the renter is ready to accept upload requests
  const { ready } = await retry(() => SiaClient.makeRequest("/renter/uploadready"));

  if (!ready) {
    logger.silly("Renter not ready for upload, skipping");

    return close();
  }

  const streamConfig = db.stream.getState();

  // check whether fswatcher is already watching this path
  if (state.fswatcher && state.localWatchPath === streamConfig.localPath) {
    logger.silly(`Path ${streamConfig.localPath} already watched, skipping`);

    return;
  }

  // close fswatcher if it is already running (for example when changing paths)
  if (state.fswatcher) {
    close();
  }

  logger.silly(`Starting file watcher`);

  state.localWatchPath = streamConfig.localPath;
  state.fswatcher = startFolderWatcher(streamConfig.localPath, (localFilePath: string) => {
    processNewFile(localFilePath, streamConfig.localPath);
  });

  logger.silly(`File watcher running ok`);
}

async function close() {
  if (state.fswatcher) {
    logger.debug(`Stopping file watcher on path ${state.localWatchPath}`);

    await state.fswatcher.close();

    state.fswatcher = null;
  }
}

function startFolderWatcher(path: string, callback): chokidar.FSWatcher {
  logger.debug(`Started folder watching on ${path}`);

  return chokidar
    .watch(path, {
      ignored: /(^|[/\\])\../, // ignore dotfiles
      awaitWriteFinish: {
        stabilityThreshold: ms("10 seconds"), // amount of time for a file size to remain stable
        pollInterval: ms("1 second"), // file stability size polling interval
      },
    })
    .on("add", callback);
}

async function processNewFile(localFilePath: string, localRootPath: string) {
  const SiaClient = getSiaClient();
  const streamConfig = db.stream.getState();

  logger.silly(`Processing new file ${localFilePath}`);

  const siaFilePath = path.posix.join(streamConfig.siaStreamSiaPath, path.relative(localRootPath, localFilePath));

  try {
    // check whether the file already exists in sia
    await SiaClient.GetFile(siaFilePath);

    logger.silly(`File ${siaFilePath} already exists, skipping upload`);

    return;
  } catch {
    // file does not exist, continue
  }

  logger.silly(`Queueing new file for upload: ${siaFilePath}`);

  try {
    // queue file for upload
    await SiaClient.Upload(siaFilePath, localFilePath);

    logger.debug(`File succesfully queued for upload ${siaFilePath}`);
  } catch (error) {
    logger.error(`Failed to queue upload of ${localFilePath} to ${siaFilePath}`, {
      error: error.message,
    });
  }
}
