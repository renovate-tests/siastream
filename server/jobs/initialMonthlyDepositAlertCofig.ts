import retry from "async-retry";
import { BLOCKS_PER_MONTH } from "sia-typescript";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete, isConsensusSynced } from "./conditions";

const logger = winston.child({ label: "initial monthly deposit alert height job" });

export default async function initialMonthlyDepositAlertCofig() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  // check if consensus is synced
  if (!(await isConsensusSynced(logger))) return;

  const streamConfig = db.stream.getState(); // get local stream config

  if (streamConfig.nextMonthlyDepositHeight) return;

  const SiaClient = getSiaClient();
  const renter = await retry(() => SiaClient.Renter()); // get current renter state

  // when syncing new siad node, currentperiod is initialized as 0
  // and we need to wait until it is set to an actual block height
  if (!renter.currentperiod) {
    logger.silly("Current period not yet settled, skipping");

    return;
  }

  // initialize next monthly deposit alert height to current period height + 1 month in blocks height
  const nextMonthlyDepositHeight = renter.currentperiod + BLOCKS_PER_MONTH;

  db.partialSet(db.stream, { nextMonthlyDepositHeight });

  logger.debug(`Next monthly deposit alert height initialized to ${nextMonthlyDepositHeight}`);
}
