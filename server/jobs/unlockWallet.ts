import retry from "async-retry";
import db from "../db";
import winston from "../lib/winston";
import { getSiaClient } from "../siaClient";
import { isSetupComplete } from "./conditions";

const logger = winston.child({ label: "wallet unlock job" });

export default async function unlockWallet() {
  // check if the user completed setup
  if (!isSetupComplete(logger)) return;

  const streamConfig = db.stream.getState(); // get local stream config

  // check if password has been provided in config
  if (!streamConfig.password) {
    logger.silly("Autounlock not enabled, skipping");

    return;
  }

  const SiaClient = getSiaClient();
  const wallet = await retry(() => SiaClient.Wallet()); // get current wallet state

  // check whether wallet has allready been encrypted
  if (!wallet.encrypted) {
    logger.silly("Wallet not yet encrypted, skipping");

    return;
  }

  // check whether wallet is already unlocked
  if (wallet.unlocked) {
    logger.silly("Wallet already unlocked, skipping");

    return;
  }

  try {
    await SiaClient.UnlockWallet(streamConfig.password);
  } catch (error) {
    logger.error("Wallet password incorrect or other unlock failure, skipping", {
      error: error.response ? error.response.body.message : error.message,
    });

    return;
  }

  logger.debug("Wallet unlocked successfully");
}
